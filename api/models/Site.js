/**
* Site.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {
  	nom:{
  		type: 'string',
  		required: true,
  	},
  	typeS:{
  		type: 'string',
  	},
  	numTelS:{
  		type: 'integer',
  	},
    adresse:{
      type: 'string',
    },
  	missionsD:{
  		collection: 'mission',
  		via: 'siteD',
  	},
    missionsA:{
      collection: 'mission',
      via: 'siteA',
    },
    positionX:{
      type: 'float',
      required: true,
    },
    positionY:{
      type: 'float',
      required: true,
    },
  }
};

