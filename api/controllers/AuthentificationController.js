/**
 * AuthentificationController
 *
 * @description :: Server-side logic for managing authentifications
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var bcrypt = require('bcryptjs');
var _userController = require('./UserController');

module.exports = {
    login: function(req,res){
        console.log("Reçus")
        var email = req.param('email');
        var password = req.param('password');
        if(!email || !password) return res.json(401,{err:'email and password are required'})
        User.findOne({email:email}, function(err,user){
            if(err)console.log(err);
            if(err) return res.json(403, {err:'forbidden'});
            if(!user){
                console.log("Erreur1"); 
                return res.json(401,{err:'invalid email or password'});
            }
            User.comparePassword(password,user, function(err,valid){
                if(err)console.log(err);
                if(err) return res.json(403, {err:'forbidden'});
                if(!valid){console.log("Erreur2"); return res.json(401,{err:'invalid email or password'});}
                token = JwtHandler.generate({email:user.email,id: user.id});
                user.token = token;
                user.save(function(err){
                    if(err){
                        console.log(err); 
                        return res.json(403, {err:'forbidden'});
                    } 
                    return res.json(
                        {
                            user: user,
                            token:token
                        }
                    )
                })
            })
        })
    },
    loginAdmin: function(req,res){
        console.log("Reçus")
        var email = req.param('email');
        var password = req.param('password');
        if(!email || !password) return res.json(401,{err:'email and password are required'})
        User.findOne({email:email}, function(err,user){
            if(err)console.log(err);
            if(err) return res.json(403, {err:'forbidden 1'});
            if(!user){
                console.log("Erreur1"); 
                return res.json(401,{err:'invalid email or password'});
            }
            if(user.nvAuto != 2){ return res.json(401,{err:"vous n'avez pas les droits nécessaires"});}
            User.comparePassword(password,user, function(err,valid){
                if(err)console.log(err);
                if(err) return res.json(403, {err:'forbidden 2'});
                if(!valid){console.log("Erreur2"); return res.json(401,{err:'invalid email or password'});}
                token = JwtHandler.generate({email:user.email,id: user.id});
                user.token = token;
                user.save(function(err){
                    if(err){
                        console.log(err); 
                        return res.json(403, {err:'forbidden 3'});
                    } 
                _userController.ListUserForManager(req,res);
                //return res.redirect('/ListUserForManager');
                })
            })
        })
    },
    refresh: function(req,res){
       var user = req.user || false;

        if(user){
            var decoded = JwtHandler.decode(user.refreshToken);
            if(decoded.email === user.email){
                token = JwtHandler.generate({email:user.email,id: user.id});
                user.token = token;
                user.save(function(err){
                    if(err) return res.json(403, {err:'forbidden'});
                    return res.json(
                        {
                            user: user,
                            token:token
                        }
                    )
                })
            }
        }else{
            return res.json(403, {err:'forbidden'});
        }
    }
};

